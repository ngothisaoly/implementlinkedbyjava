package Exam;

public class test {
	public static void main(String[] args) {    
        LinkedListImpl<Integer> llists = new LinkedListImpl<>();
        llists.append(1);
        llists.append(2);
        llists.append(3);
        llists.append(4);
        llists.append(5);
        llists.append(6);
        System.out.println("List");     
        llists.printList();
        llists.deleteNode(2);
        System.out.println("After delete");
        llists.printList();
        llists.deleteAll(); 
        System.out.println("After delete all");
        llists.printList();
	}

}
