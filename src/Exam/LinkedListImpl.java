package Exam;

public class LinkedListImpl<T>  implements LinkedList<T>{
	
	Node<T> head;
	
	public Node<T> getHead() {
		return head;
	}

	public void setHead(Node<T> head) {
		this.head = head;
	}
	
	public void printList() {
		Node<T> n = head;
		while (n != null) {
			System.out.println("data: " + n.getData());
			n = n.next;
		}
	}

	@Override
	public void addAtTop(T data) {
		Node<T> newNode = new Node<>(data);
		newNode.next =  this.head;
		this.head = newNode;
	}

	@Override
	public void append(T data) {
		Node<T> newNode = new Node<>(data);
		if (head == null) {
			head =  newNode;
			return;
		}
		newNode.next = null;
		Node<T> lastNode = this.head;
		while(lastNode.next != null) {
			lastNode = lastNode.next;
		}
	    lastNode.next = newNode;
	}
	
	
	@Override
	public void deleteNode(int position) {
		if(this.head == null) {
			return;
		}
		Node<T> temp = this.head;
		if(position == 0) {
			this.head = temp.next;
			return;
		}
        for (int i=0; temp!=null && i<position-1; i++)
            temp = temp.next;
        if (temp == null || temp.next == null)
            return;
        Node<T> next = temp.next.next;
        temp.next = next; 
	}
	
	@Override
	public void deleteAll() {
		this.head = null;
	}
}
