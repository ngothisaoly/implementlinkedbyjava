package Exam;

public interface LinkedList<T> {
	public void addAtTop(T data) ;
	public void append(T data);
	public void deleteNode(int position);
	public void deleteAll();
}
